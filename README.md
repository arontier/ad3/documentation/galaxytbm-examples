# arontier/ad3/documents/galaxytbm-examples

This is a repository for galaxytbm app example files.

## Prerequisites

You should have 

* `<api-key-string>`: API key of your AD3 REST API client machine
* `<token-string>`: a user's auth token

to consume AD3 REST API endpoints. And
you should do the following demonstration on your AD3 REST API client machine.

## How to use

### Getting examples

First git-clone this repository and change directory into it:

```bash
git clone https://gitlab.com/arontier/coworkers/cimplrx/galaxytbm-examples
cd galaxytbm-examples/examples
```

### Submitting work
To submit a work, run:

```bash
curl --location --request POST 'https://rest.ad3.io/api/v1/galaxytbm/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>' \
  --form 'description=testing-galaxytbm' \
  --form 'fasta=@input/T0949.fa'
```

If our AD3 REST API server is not heavily loaded, it will finish this example in about 100 minutes.

### Listing works

To list all submitted works, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/galaxytbm/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

### Getting work back

To see specific work, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/galaxytbm/works/<work-id>/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

where

* `<work-id>` is the value of `id` returned in the response of "listing works" API request.

A download link `outputs_link` for your submitted work is in the response of the above request.
Download the result using this link and compare with the one in `output` directory.

## References

* REST API Documentation: [https://docs.ad3.io/#/galaxytbm](https://docs.ad3.io/#/galaxytbm)

You may need a read access privilege to see the following references:

* Docker, workflow repository: [https://gitlab.com/arontier/ad3/galaxytbm](https://gitlab.com/arontier/ad3/galaxytbm)
